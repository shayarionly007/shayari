Love is the most beautiful emotion of all, but not everyone is lucky to receive it back. 
One experiences an indescribable pain when a lonely heart aches for someone who will never feel the same for you. 
You long to hold the hand that will never reach out for you and your heart silently suffers through the pangs of loneliness. 
The agony of unrequited love is indescribable, which also makes it a favourite topic for writers and poets.  

Here are some hauntingly beautiful shayaris and couplets for the love that could never be yours.
shayari
sad shayari
love shayari
https://shayarionly.com